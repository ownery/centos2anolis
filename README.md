# CentOS 迁移 Anolis OS

该工具用于自动化就地迁移CentOS实例至Anolis OS实例。

## 支持版本

该工具支持迁移CentOS Linux 7和8，不支持CentOS Stream。

## 迁移准备

**注意事项:**
工具不是完美的，不可能处理所有可能的情况。
1. 请确认在执行迁移前做好系统备份。
1. 确认CentOS的yum/dnf配置可用。
1. 保证`/var/cache` 有至少5GB空间。
1. 禁用所有的自动更新，比如`yum-cron` 需要禁止。
1. 确认CentOS系统没有安装i686软件包。

## 使用介绍

1. 使用root运行。
1. 确认系统安装python3。
1. 下载工具[`centos2anolis.py`][2]。
1. 运行`python3 centos2anolis.py` 开始迁移。

## 选项介绍

* `-c` 继续迁移

    迁移中断时，比如迁移过程中执行 `yum distro-sync` 意外中断，则可以使用该选项继续迁移。

* `-V` 校验迁移前后的软件包

    该选项会在`/var/tmp/` 目录下创建四个文件：

    * `${hostname}-rpms-list-[before|after].log`: 迁移前后的系统软件包安装列表。
    * `${hostname}-rpms-verified-[before|after].log`: 迁移前后系统安装软件包校验结果。

* `-s` 加速选项

    工具默认从mirrors.openanolis.org下载软件包，有些场景下下载缓慢。

    该选项使用mirrors.aliyun.com加速下载。

* `--log_dir` 设置自定义日志路径

    该选项会在用户自定义路径下创建日志文件`centos2anolis.log`，不使用该选项则会在默认路径`/var/log/`下创建日志`centos2anolis.log`。

* `--progress_file` 设置自定义执行状态信息json文件

    该选项会在用户自定义json文件路径下创建文件，不使用该选项则创建默认文件`/var/log/centostoanolis.json`。

* `-l` 离线迁移

    在无法接入到mirrors.openanolis.org 或 mirrors.aliyun.com时，可以使用本地源迁移系统。
    
    在运行工具之前，新建或者编辑Anolis仓库的repo文件：/etc/yum.repos.d/switch-to-anolis.repo , 将对应的 repo baseurl 地址改成本地源 repo 地址， 譬如，假设本地源地址为 http://local.repo.com/anolis ,
    则配置文件如下

    ```
    [an8_baseos]
    name=AnolisOS-8 - BaseOS
    baseurl=http://local.repo.com/anolis/8/BaseOS/$basearch/os
    gpgcheck=0
    enabled=1

    [an8_appstream]
    name=AnolisOS-8 - AppStream
    baseurl=http://local.repo.com/anolis/8/AppStream/$basearch/os
    gpgcheck=0
    enabled=1
    ```
* `--tool_version` 查看脚本版本

    该选项会返回当前脚本`centos2anolis.py`的版本号。

* `--rhck` 指定迁移内核为RHCK

    该选项会将内核迁移为RHCK，而非默认的ANCK。

## 错误码
- 成功
   - 000 : 执行成功无报错
- 迁移环境检测失败
   - 101 : 迁移工具的运行环境不满足，需要用户处理后重试
   - 102 : 迁移前OS或软件版本不合理，需要用户处理后重试
   - 103 : 环境不满足安装迁移工具条件，安装迁移工具失败
   - 199 : 其它原因导致的迁移环境检测失败
- RPM包替换失败
   - 201 : yum配置错误
   - 202 : RPM包下载与安装失败
   - 203 : 设置repo状态失败
   - 299 : 其它原因导致的RPM包替换失败
- 迁移后设置失败
   - 301 : 迁移后验证失败
   - 302 : 设置启动项失败
   - 399 : 其它原因导致的迁移后设置失败
- 杂项
   - 999 : 其它原因导致的迁移失败
       
## 问题反馈

   任何问题，需求或者疑问，都可以在[bugzilla.openanolis.cn][1]上提交。

   工具运行过程的日志记录在`/var/log/centos2anolis.log`，反馈问题时可附上日志文件。

## License

Copyright (c) 2021-2022 OpenAnolis Community

查看LICENSE.txt获取更多信息。

[1]: https://bugzilla.openanolis.cn
[2]: https://gitee.com/anolis/centos2anolis/raw/master/centos2anolis.py
