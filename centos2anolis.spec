%define anolis_release 20

Name:           centos2anolis
Version:        0.2
Release:        %{anolis_release}%{?dist}
Summary:        Transfer CentOS to Anolis.

License:        MulanPSL2
URL:            www.openanolis.cn
Source0:        %{name}.py
Source1:        README.md
Source2:        LICENSE.txt

BuildRequires:  python3
Requires:       python3
Requires:       bash
Requires:       yum-utils
%if 0%{rhel} == 7
Requires:       python36-psutil
%else
Requires:       python3-psutil
%endif

BuildArch:      noarch

%description
This script is designed to automatically convert a CentOS instance to 
Anolis OS in-place. Currently it supports transferring from CentOS 7 
to Anolis OS 7 and from CentOS 8 to Anolis OS 8.

%prep
cp %{SOURCE1} .
cp %{SOURCE2} .


%install
mkdir -p %{buildroot}/%{_sbindir}
install -m 755 %{SOURCE0} %{buildroot}/%{_sbindir}


%files
%{_sbindir}/%{name}.py
%doc README.md
%license LICENSE.txt


%changelog
* Thu Mar 22 2023 Weitao Zhou <yunqi.zwt@alibaba-inc.com> - 0.2-20
- append dist info to Release

* Tue Mar 21 2023 Weitao Zhou <yunqi.zwt@alibaba-inc.com> - 0.2-19
- To ensure the migration, this machine must have 512MB RAM at least totally

* Fri Feb 24 2023 Weisson <Weisson@linux.alibaba.com> - 0.2-18
- Correct python3-psutil requirement branch make migration ARM-EPEL-compatible.

* Tue Feb 14 2023 Weitao Zhou <yunqi.zwt@alibaba-inc.com> - 0.2-17
- improvement: set the memory size limit as 300M

* Thu Feb 9 2023 Bitao Hu <hubitao.hbt@alibaba-inc.com> - 0.2-16
- [Bugfix] Can not set custom json file when use check.

* Fri Feb 3 2023 Bitao Hu <hubitao.hbt@alibaba-inc.com> - 0.2-15
- Support perform a feasibility check on the migration environment.

* Thu Feb 2 2023 Rui Huang <meiyou.hr@alibaba-inc.com> - 0.2-14
- Check if the filesystem is mounted as readonly.

* Wed Jan 18 2023 Weisson <Weisson@linux.alibaba.com> - 0.2-13
- Perform system requirements check to ensure migration.

* Thu Jan 12 2023 Rui Huang <meiyou.hr@alibaba-inc.com> - 0.2-12
- Add rpm database check and fix before migration.

* Tue Jan 10 2023 Weitao Zhou <yunqi.zwt@alibaba-inc.com> - 0.2-11
- [Bugfix] failed to run without public network.

* Wed Dec 28 2022 Bitao Hu <yaoma@linux.alibaba.com> - 0.2-10
- Modify the length of errMsg to 1024.

* Thu Dec 22 2022 Bo Ren <rb01097748@alibaba-inc.com> - 0.2-9
- Make the repo status same before and after migration

* Mon Dec 19 2022 Bitao Hu <yaoma@linux.alibaba.com> - 0.2-8
- Return diffrent error code on diffrent excepts

* Wed Nov 23 2022 Weitao Zhou <yunqi-zwt@linux.alibaba.com> - 0.2-7
- remove func resume_third_repos

* Wed Nov 23 2022 Weisson <Weisson@linux.alibaba.com> - 0.2-6
- [Bugfix] Third-party repository may not be catched when there are several.

* Fri Nov 4 2022 Weisson <Weisson@linux.alibaba.com> - 0.2-5
- Add requirement of yum-utils to make sure that repoquery exists.

* Thu Oct 20 2022 Weisson <Weisson@linux.alibaba.com> - 0.2-4
- Support third-party packages to ensure the migration process.
- [BUGFIX] Internal ECS visit repository by http.

* Thu Sep 8 2022 mgb01105731 <mgb01105731@alibaba-inc.com> - 0.2-3
- Set upgraded default kernel type to ANCK 

* Tue Sep 6 2022 mgb01105731 <mgb01105731@alibaba-inc.com> - 0.2-2
- support upgrade kernel to ANCK

* Fri Jun 10 2022 mgb01105731 <mgb01105731@alibaba-inc.com> - 0.2-1
- Add --tool_version to check the version of tool

* Thu May 26 2022 XueZhixin <xuezhixin@uniontech.com> - 0.1-6
- Add offline migration

* Thu May 26 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-5
- add custom log path
- add custom json file

* Fri Mar 25 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-4
- fix module virt

* Fri Feb 11 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-3
- Add mysql-server check
- handle 3rd-part repository files and try using an internal web address
- Add -c usage to continue migration
- save print message to /var/log/centos2anolis.log
- update README and LICENSE
- optimise code style
- add default version for switch

* Wed Jan 05 2022 Chunmei Xu <xuchunmei@linux.alibaba.com> - 0.1-2
- add check for i686 packages

* Wed Dec 22 2021 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-1
- First CentOS2Anolis Package
